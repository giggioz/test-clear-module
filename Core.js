module.exports = class Core {
    constructor(delegates) {
        this._delegates = delegates
    }
    getPoints() {
        return this._delegates.getPoints()
    }
}
