const clearModule = require('clear-module')

const Core = require('./Core')
const delegates = require('./delegates')

const core = new Core(delegates)

let id = 0
setInterval(() => {
    clearModule('./delegates')
    core._delegates = require('./delegates')
    console.log(id++, core.getPoints())
}, 500)
